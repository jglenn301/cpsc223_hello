#include <stdio.h>

int main(int argc, char **argv)
{
  printf("Hello, world!\n");
  printf("\n");
  printf("My name is Jim Glenn and I have a thing or two to say about\n");
  printf("scramble crossings.  Part of the point of a scramble crossing\n");
  printf("is to reduce conflicts between pedestrians and turning\n");
  printf("traffic.  So, since they get a whole part of the cycle to\n");
  printf("themselves, pedestrians should please heed the \"Don't Walk\"\n");
  printf("sign when traffic is trying to turn.  It is hard enough to\n");
  printf("drive or ride through downtown without pedestrians misbehaving.\n");
  printf("\n");
  printf("I also committed mass herbicide again this summer.\n");
}
